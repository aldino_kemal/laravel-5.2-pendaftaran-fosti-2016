<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Pendaftaran;

use Validator, Redirect, Session, Mail, Input, DB, NoCaptcha;

use Intervention\Image\ImageManagerStatic as Image;


class HomePostController extends Controller
{
    public function kirim(Request $request)
    {	

    	$rules = [
            'email' => 'required|email',
            'jurusan' => 'required',
            'nim' => 'required|min:10|max:10',
            'angkatan' => 'required',
            'nama' => 'required|string',
            'no_telepon' => 'required',
            'hoby' => 'required',
            'motivasi' => 'required',
            'asal_sekolah' => 'required',
            'alamat_rumah' => 'required',
            'gender' => 'required',
            'tahun_lahir' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg|max:1024',
            'ktm' => 'required|image|mimes:jpeg,png,jpg|max:500',
            'persetujuan' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ];


        $this->validate($request, $rules);

        // NoCaptcha::shouldReceive('verifyResponse')->once()->andReturn(true);
        
        // // provide hidden input for your 'required' validation
        // NoCaptcha::shouldReceive('display')->zeroOrMoreTimes()->andReturn('<input type="hidden" name="g-recaptcha-response" value="1" />');

    	// Dokumen Umum
    	// echo $request['email'];
    	// echo "<br>";
    	// echo $request['jurusan'];
    	// echo "<br>";
    	// echo $request['nim'];
    	// echo "<br>";
    	// echo $request['angkatan'];
    	// echo "<br>";

    	// // Informasi pribadi
    	// echo $request['nama'];
    	// echo "<br>";
    	// echo $request['no_telepon'];
    	// echo "<br>";
    	// echo $request['hoby'];
    	// echo "<br>";
    	// echo $request['motivasi'];
    	// echo "<br>";
    	// echo $request['asal_sekolah'];
    	// echo "<br>";
    	// echo $request['alamat_rumah'];
    	// echo "<br>";
    	// echo $request['gender'];
    	// echo "<br>";
    	// echo $request['tanggal_lahir'];
    	// echo "<br>";
    	// echo $request['bulan_lahir'];
    	// echo "<br>";
    	// echo $request['tahun_lahir'];
    	// echo "<br>";
    	// echo $request['persetujuan'];

        // session->flush() : menghapus semua session
        Session()->flush();
    	Session::put('nama', strtoupper($request['nama']));
    	Session::put('email', $request['email']);
    	Session::put('nim', strtoupper($request['nim']));
        Session::put('user',strtoupper($request['nim']));
    	
// =============================== Upload FOTO =============================== //
		// pasfoto_ori ( BUAT ID CARD )
    	// $image_pasfoto = $request->file('foto');
     //    $input['imagename'] = strtoupper($request['nim']).'_'.strtoupper($request['nama']).'.'.$image_pasfoto->getClientOriginalExtension();
     //    $destinationPath = public_path('images/pasfoto_ori');
     //    $img = Image::make($image_pasfoto->getRealPath());
     //    $img->save($destinationPath.'/'.$input['imagename']);


    	// pasfoto
  //   	$image_pasfoto = $request->file('foto');
  //       $input['imagename'] = strtoupper($request['nim']).'.'.'jpg';
  //       $destinationPath = public_path('images/pasfoto');
  //       $img = Image::make($image_pasfoto->getRealPath());
  //       $img->resize(150, 150, function ($constraint) {

		//     $constraint->aspectRatio();

		// })->save($destinationPath.'/'.$input['imagename']);

        // pasfoto di PUBLIC_HTML
        // $image_pasfoto = $request->file('foto');
        // $input['imagename'] = strtoupper($request['nim']).'.'.'jpg';
        // $destinationPath = public_path('/../../pendaftaran.fostiums.com/images/pasfoto');
        // $img = Image::make($image_pasfoto->getRealPath());
        // $img->resize(150, 150, function ($constraint) {

        //     $constraint->aspectRatio();

        // })->save($destinationPath.'/'.$input['imagename']);

        // KTM
        // $image_pasfoto = $request->file('ktm');
        // $input['imagename'] = strtoupper($request['nim']).'.'.$image_pasfoto->getClientOriginalExtension();
        // $destinationPath = public_path('images/ktm');
        // $img = Image::make($image_pasfoto->getRealPath());
        // $img->save($destinationPath.'/'.$input['imagename']);
                

// ========================= Save to Database =========================== //
        $daftar = new Pendaftaran();
        try{
            $daftar->nim = strtoupper($request['nim']);
            $daftar->email = $request['email'];
            $daftar->jurusan = strtoupper($request['jurusan']);
            $daftar->angkatan = $request['angkatan'];
            $daftar->nama = strtoupper($request['nama']);
            $daftar->no_telepon = $request['no_telepon'];
            $daftar->hoby = $request['hoby'];
            $daftar->motivasi = $request['motivasi'];
            $daftar->asal_sekolah = strtoupper($request['asal_sekolah']);
            $daftar->alamat_rumah = strtoupper($request['alamat_rumah']);
            $daftar->gender = strtoupper($request['gender']);
            $daftar->tanggal_lahir = $request['tahun_lahir'].'-'.$request['bulan_lahir'].'-'.$request['tanggal_lahir'];


            // dd($daftar);
			$daftar->save();
		}catch  (\Illuminate\Database\QueryException $e) {
            // dd($e);
		   return redirect()->back()->withErrors(['DBError'=>'NIM Telah Terdaftar']);
        }

        
// ============================= Kirim Email =============================== //
        $data = ['nama' => Session::get('nama'),
                 'pdf' => url('/berhasil/'.Session::get('nim').'/'.md5('formulirpendaftaran'))];

        Mail::send('send', $data, function ($message) {
            $message->from('fostiums2007@gmail.com', 'FOSTI UMS');
            $message->to(Session::get('email'))->subject('Open Rec FOSTI 2016');
        });

        return redirect('/berhasil/'.strtoupper($request['nim']));
    }
}
