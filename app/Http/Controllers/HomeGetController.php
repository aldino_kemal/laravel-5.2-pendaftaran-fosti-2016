<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;

class HomeGetController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function caradaftar()
    {
        return view('caradaftar');
    }

    public function berhasil($nim)
    {	
    	// if (Session()->has('admin')) {
     //        return redirect('admin');
     //    }
    	
        if (Session()->has('user')) {
        	if(Session::get('user') == $nim){
        		return view('sucess_reg')->with(['nama'=>Session::get('nama'), 'nim'=>Session::get('nim')]);	
        	}
        	// kalau orang mau masuk ke nim lain 
        	return redirect('berhasil/'.Session::get('user'));            
        }

        // tidak memiliki session
        return redirect('/');
    }
}
