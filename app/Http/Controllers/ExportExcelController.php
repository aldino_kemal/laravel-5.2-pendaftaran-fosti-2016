<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB, Excel;

class ExportExcelController extends Controller
{
    public function export()
    {
		Excel::create('DATABASE Calon FOSTI 2016', function($excel) {

	    $excel->sheet('DATABASE FOSTI', function($sheet) {
	    	$sheet->setOrientation('landscape');
	        // AKSES database
	    	$datas = DB::table('pendaftarans')->get();

		    $isi_excel = [['NIM', 'NO HP', 'NAMA', 'KELAMIN', 'JURUSAN']];

		    $isi_sementara = [];
		    foreach ($datas as $data) {
				array_push($isi_sementara, $data->nim);
				array_push($isi_sementara, $data->no_telepon);
				array_push($isi_sementara, $data->nama);
				array_push($isi_sementara, $data->gender);
				array_push($isi_sementara, $data->jurusan);
				array_push($isi_excel, $isi_sementara);
				$isi_sementara = [];
		    }

		    // dd($isi_sementara);
		    // proses pemasukan (isi_excel,null,awal kolom, false, header range)
	        $sheet->fromArray($isi_excel,null, 'A1', false, false);

	    	});
		})->download('xls');
    	
    }
}
