<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use PDF, Session, DB;

use App\Pendaftaran;

class PdfController extends Controller
{
    public function index($nim)
    {	
    	$database = DB::table('pendaftarans')->where('nim',$nim)->first();
        if(is_null($database)){
            return view('errors/404');
        }

        $pdf=PDF::loadView('filepdf',['nim'=>$database->nim,
                                          'nama'=>$database->nama,
                                          'jurusan'=>$database->jurusan,
                                          'angkatan'=>$database->angkatan,
                                          'motivasi'=>$database->motivasi,
                                        ]);
            // $pdf->stream('FormulirPendaftaran.pdf'); 
            return $pdf->download('FormReg_'.$database->nim.'_'.$database->nama.'.pdf');       

    	return redirect('/');
    }
}
