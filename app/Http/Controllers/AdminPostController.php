<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Requests;

use Session, Auth, Cache, File, DB;



class AdminPostController extends Controller
{
	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    public function login(Request $request){    	

    	$this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user_data = [
        			 'username' => $request['username'],
                     'password' => $request['password'],
                     ];

        // $username = $request->old('username');
        // dd($username);

        if (Auth::attempt($user_data)) {
            	Session::put('admin', Auth::user()->username);
                Cache::forget('ip_tersimpan');
            	return redirect('/admin/home');
        }

        if (Cache::has('ip_tersimpan')){
        	$coba_login = Cache::get('coba_login');
        	// dd($coba_login);
        	if($coba_login < 4){
        		$coba_login = $coba_login + 1;
        		Cache::put('coba_login',$coba_login,1);
        	}else{
        		Cache::put('suspen_',$request->ip(),1);
        		return redirect('admin');
        	}

        }else{
        	$ip = $request->ip();
	        // ip tersimpan
	        Cache::put('ip_tersimpan', $ip, 1);
	        Cache::put('coba_login', 1, 1);	
        }

        

        return redirect()->back()->withErrors(['data_error'=>'Username atau Password Salah'])->withInput($request->except("password"));

    }

    public function delete($nim)
    {   
        // Delete from database
        DB::table('pendaftarans')->where('nim', '=', $nim)->delete();

        // Delete Image Paspoto
        $files_pasfoto = glob(public_path('images/pasfoto/'.$nim.'.*'));
        foreach ($files_pasfoto as $file) {
            File::delete($file);
        }

        // Delete Image Paspoto PUBLIC_HTML
        $files_pasfoto = glob(public_path('/../../pendaftaran.fostiums.com/images/pasfoto/'.$nim.'.*'));
        foreach ($files_pasfoto as $file) {
            File::delete($file);
        }

        // Delete Image KTM
        $files_ktm = glob(public_path('images/ktm/'.$nim.'.*'));
        foreach ($files_ktm as $file) {
            File::delete($file);
        }

        // Delete Image Paspoto_ORI
        $files_pasfoto = glob(public_path('images/pasfoto_ori/'.$nim.'_*'));
        foreach ($files_pasfoto as $file) {
            File::delete($file);
        }

        return redirect('/admin');
    }

    public function logout(Request $request)
    {
        Session()->flush();
        return redirect('/admin');
    }
}
