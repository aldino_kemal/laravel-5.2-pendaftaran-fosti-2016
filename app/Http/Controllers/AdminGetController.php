<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB, Cache;

class AdminGetController extends Controller
{
    public function login(Request $request){
    	
        if (Cache::has('suspen_')){
            if ($request->ip() == Cache::get('ip_tersimpan')){
            // dd(Cache::get('ip_tersimpan'));
                return redirect('/whoareyou?');
            } 
        }
        else{
            if (Session()->has('admin')) {
                return redirect('admin/home');
            }
        }
        return view('admin.login');
    	
    }

    public function home(){
        if (Session()->has('admin')) {
        	$datas = DB::table('pendaftarans')->orderBy('created_at')->get();
        	return view('admin.home')->with(['datas'=>$datas]);
        }else{
            return redirect('admin');
        }
    }

    public function detail($nim){
        if (Session()->has('admin')) {
            $datas = DB::table('pendaftarans')->where('nim',$nim)->first();
            // dd($datas);
            if (is_null($datas)){
                return view('errors/404');
            }
            return view('admin.detail')->with(['nim'=>$nim, 'datas'=>$datas]);
        }
        else{
            return redirect('admin');
        }
       
    }

    public function delete($nim){
        if (Session()->has('admin')) {
            $datas = DB::table('pendaftarans')->where('nim',$nim)->first();
            if (is_null($datas)){
                return view('errors/404');
            }
            return view('admin.delete')->with(['nim'=>$nim, 'datas'=>$datas]);
        }
        else{
            return redirect('admin');
        }
       
    }

    public function whoareyou(){
        if (Cache::has('suspen_')){
    	   return view('admin.whoareyou');
        }
        return view('errors.404');
    }
}
