<?php

Route::get('closed', function() {
    return view('closed');
});



Route::group(['middleware' => ['web']], function () {
    // index
    Route::get('/', 'HomeGetController@index');
    
    // Route::get('/hidden', 'HomeGetController@index');

    // Route::get('/', function(){
    //     return redirect('closed');
    // });

	Route::post('/', 'HomePostController@kirim');

        // cara daftar
	Route::get('/caradaftar', 'HomeGetController@caradaftar');

	// setelah berhasil daftar
	Route::get('berhasil/{nim}', 'HomeGetController@berhasil');

	// menampilkan file pdf (didownload)
	Route::get('/berhasil/{nim}/'.md5('formulirpendaftaran'), 'PdfController@index');

	// admin login
	Route::get('/admin','AdminGetController@login');
	Route::post('/admin','AdminPostController@login');

	// home admin
	Route::get('/admin/home','AdminGetController@home');
	Route::post('/admin/home','AdminPostController@logout');

	// export excel
	Route::get('/admin/exportExcel', 'ExportExcelController@export');
	
	// admin detail
	Route::get('/admin/detail/{nim}','AdminGetController@detail');

	// admin delete
	Route::get('/admin/delete/{nim}','AdminGetController@delete');	
	Route::post('/admin/delete/{nim}','AdminPostController@delete');

	// login spam
	Route::get('/whoareyou','AdminGetController@whoareyou');	



});





	// latihan
	// Route::get('resizeImage', 'ImageController@resizeImage');

	// Route::post('resizeImagePost',['as'=>'resizeImagePost','uses'=>'ImageController@resizeImagePost']);


