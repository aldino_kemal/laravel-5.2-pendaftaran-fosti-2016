<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

use Illuminate\Database\Eloquent\Model;

class Login_admin extends Model implements Authenticatable
{
	use AuthenticableTrait;
    protected $table = 'login_admins';

    // agar bisa masukkan banyak data langsung
    protected $fillable = ['username','password'];

    protected $primaryKey = 'username';
}
