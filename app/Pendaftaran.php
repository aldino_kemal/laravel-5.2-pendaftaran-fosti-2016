<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
	// untuk menghubungkan jika tidak plural
    protected $table = 'pendaftarans';

    // agar bisa masukkan banyak data langsung
    protected $fillable = ['nim', 'email','jurusan','angkatan','nama','no_telepon','hoby','motivasi','asal_sekolah','alamat_rumah','gender','tanggal_lahir'];

    protected $primaryKey = 'nim';

}
