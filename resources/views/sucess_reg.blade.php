<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Selamat</title>	
	<link href="{{ asset('images/icn.png') }}" rel="shortcut icon" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=0.5">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/sucess.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
</head>
<body>
	<p class="selamat">
		<i class="fa fa-check label-rounded" aria-hidden="true" style="color:#46C67B"></i><br><br>
		Selamat {{ $nama }} kamu berhasil mendaftar sebagai calon anggota F<font color='#FC2C2C'>OS</font>TI 2016<br>
		bawa <strong>formulir pendaftaran</strong> saat kegiatan Open Recruitmen berlangsung yang bisa di download dibawah<br> <span style="font-size:15px">Cek e-mail kamu sekarang</span><br>
		<a href={{url('berhasil/'.$nim.'/'.md5('formulirpendaftaran'))}} target="_blank" class="btn btn-success">Download</a>
		<br>
		
	</p>

	<center>
		<div id="footer" style="color: #FFFFFF; background-color: #364447; padding-top: 5px; padding-bottom: 5px;">
	         
	        <a href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true" style="font-size:30px; color:#F0F0F0; padding-top: 10px "></i></a>
	        <br>
	         <h3>Kembali ke halaman awal</h3> 
			<br>
			follow us<br>
	        <a href="https://www.instagram.com/fosti_ums/" target="_blank"><i class="fa fa-instagram" aria-hidden="true" style="font-size:20px; color:#F0F0F0; margin-right:20px"></i></a>	        
	        <a href="https://twitter.com/fostiums" target="_blank"><i class="fa fa-twitter" aria-hidden="true" style="font-size:20px; color:#F0F0F0;"></i></a>
		</div>
	</center>
	
</div>
</body>
</html>