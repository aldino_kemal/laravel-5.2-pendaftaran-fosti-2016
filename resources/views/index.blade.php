<!DOCTYPE html>
    <head>
        <title>Pendaftaran FOSTI</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link href="{{ asset('images/icn.png') }}" rel="shortcut icon" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="{{asset('css/index/default.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery/jquery-ui.css') }}">
        <script src="{{ asset('css/jquery/jquery-1.12.4.js')}}"></script>
        <script src="{{ asset('css/jquery/jquery-ui.js') }}"></script>
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <noscript>
        <center>        
            <div style="margin-top:200px; font-size:30px">Enable JavaScript for best performance</div> 
            <style>form { display:none; }</style>
        </center>
        </noscript>

    </head>
    <body> 
    <center><marquee behavior='alternate' scrolldelay="200" width="800"> Pendaftaran akan ditutup tanggal <b  style='color:#ABDA16;'>23 September 2016</b> | CP 089685024091 | Atau langsung kesekre FOSTI</b> </marquee>
    </center>
    <div>
        <form action="{{action('HomePostController@kirim')}}" class="register" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
        {!! csrf_field() !!}
            <h1 style="float:left">Pendaftaran F<font color="#abda0f">OS</font>TI 2016</h1>
            <h4 style="float:right; padding-right:10px"><a href="{{ url('caradaftar') }}" target="_blank"> Cara Daftar</a></h4>
            @if (count($errors) > 0)
                <div style="padding-left:40px; font-size:13px">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
            <fieldset class="row1">
                <legend>Data Penting
                </legend>
                <p>
                    <label >Email *
                    </label>
                    <input type="text" name="email" title="E-mail aktif" value="{{ old('email') }}" required>
                    <label>NIM *
                    </label>
                    <input type="text" name="nim" value="{{ old('nim') }}" required/>
                </p>
                <p>
                    <label>Jurusan *
                    </label>
                    <input type="text" name="jurusan" value="{{ old('jurusan') }}" required/>
                    <label>Angkatan *
                    </label>
                    <input type="text" name="angkatan" maxlength="4" value="{{ old('angkatan') }}" required/>
                    <label class="obinfo">* Wajib diisi
                    </label>
                </p>
            </fieldset>
            <fieldset class="row2">
                <legend>Data Pribadi
                </legend>
                <p>
                    <label>Nama *
                    </label>
                    <input type="text" class="long" name="nama" title="Nama lengkap tanpa singkatan" value="{{ old('nama') }}" required/>
                </p>
                <p>
                    <label>No Telepon *
                    </label>
                    <input type="text" maxlength="15" name="no_telepon" title="Nomor yang aktif" value="{{ old('no_telepon') }}" required/>
                </p>
                <p>
                    <label>Hoby *
                    </label>
                    <input type="text" class="long" name="hoby" title="Dapat lebih dari satu, pisahkan dengan tanda koma ( , )" value="{{ old('hoby') }}" required/>
                </p>
                <p>
                    <label>Motivasi *
                    </label>
                    <textarea name="motivasi" cols="25" rows="4" placeholder="Motivasi kamu bergabung dengan FOSTI ..." required></textarea>
                </p>
                <p>
                    <label>Asal Sekolah *
                    </label>
                    <input class="long" type="text" name="asal_sekolah" value="{{ old('asal_sekolah') }}" required/>

                </p>
                <p>
                    <label>Alamat Rumah *
                    </label>
                    <input class="long" type="text" name="alamat_rumah" title="Bukan alamat kost" value="{{ old('alamat_rumah') }}" required/>

                </p>
            </fieldset>
            <fieldset class="row3">
                <legend>Data Pribadi
                </legend>
                <p>
                    <label>Gender *</label>
                    <input type="radio" value="Laki" name="gender" />
                    <label class="gender">Laki</label>
                    <input type="radio" value="Perempuan" name="gender" />
                    <label class="gender">Perempuan</label>
                </p>
                <p>
                    <label>Tgl Lahir *
                    </label>
                    <select class="date" name="tanggal_lahir">
                        @for($i = 1; $i < 10; $i++)
                        <option value="0{{ $i }}">0{{ $i }}</option>
                        @endfor
                        @for($x = 10; $x < 32; $x++)
                        <option value="{{ $x }}">{{ $x }}</option>
                        @endfor
                    </select>
                    <select name="bulan_lahir">
                        <option value="1">January
                        </option>
                        <option value="2">February
                        </option>
                        <option value="3">March
                        </option>
                        <option value="4">April
                        </option>
                        <option value="5">May
                        </option>
                        <option value="6">June
                        </option>
                        <option value="7">July
                        </option>
                        <option value="8">August
                        </option>
                        <option value="9">September
                        </option>
                        <option value="10">October
                        </option>
                        <option value="11">November
                        </option>
                        <option value="12">December
                        </option>
                    </select>
                    <input class="year" type="text" size="4" maxlength="4" name="tahun_lahir" required/>
                </p>
                <div class="infobox">
                    <!-- <br><br><br><br><br> -->
                    <input type="file" name="foto" style="width:200px"> 
                    <br><br>
                    FOTO dengan ekstensi .jpg/jpeg/png - Max 1024KB
                    <br><br>
                    ______________________________________________________________________________
                    <br><br>
                    <input type="file" name="ktm" style="width:200px">
                    <br><br>
                    KTM dengan ekstensi .jpg/jpeg/png - Max 500KB
                    <!-- <h3>Cantumkan fotocopy KTM / KTM sementara pada formulir pendaftaran</h3> -->
                </div>
            </fieldset>

            <fieldset class="row4">
                <legend>Persetujuan
                </legend>
                <p class="agreement">
                    <input type="checkbox" value="persetujuan" name="persetujuan" required/>
                    <label>Saya berjanji akan menjadi anggota aktif FOSTI dan mengikuti serangkaian acara open recruitment beserta makrab dengan baik</label><br><br><br><br>
                </p>
                    <div style="padding-left:20px"> {!! app('captcha')->display(); !!} </div>
                
            </fieldset>
            <div><button class="button">Daftar &raquo;</button></div>
        </form>
    </div>

        <script>
        $( function() {
            $( document ).tooltip();
        } );
    </script>
    </body>
</html>





