Selamat {{ $nama }}, kamu berhasil mendaftar sebagai calon anggota FOSTI 2016. Ikutilah rangkaian Open Rec FOSTI 2016 dan tunggu jarkoman info selanjutnya :D
<br>
[+]Lampirkan Fotocopy KTM / KTM sementara pada formulir Open Recruitment
<br>
[+]Bawa <strong>Formulir Pendaftaran</strong> saat acara Open Recruitment berlangsung | <a href="{{ $pdf }}"> Download </a> 
<br>
[+]Follow instagram FOSTI => <a href="https://www.instagram.com/fosti_ums">@fosti_ums</a>
<br>
[+]Follow twitter FOSTI =><a href="https://twitter.com/fostiums">@fostiums</a>
<br><br><br>

<hr>
<br>
Jika ada masalah hubungi 
<br>
+6289 685 024 091 (Aldino Kemal)
<br>
atau
<br>
aldinokemal2104@gmail.com 
<br>
fostiums@gmail.com
