<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CLOSED</title>
	<link href="{{ asset('images/icn.png') }}" rel="shortcut icon" type="image/x-icon">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<style type="text/css">
		.zoom_img img{
			height:400px;
			-moz-transition:-moz-transform 0.5s ease-in; 
			-webkit-transition:-webkit-transform 0.5s ease-in; 
			-o-transition:-o-transform 0.5s ease-in;
		}
		.zoom_img img:hover{
			-moz-transform:scale(1.5); 
			-webkit-transform:scale(1.5);
			-o-transform:scale(1.5);
		}
	</style>
</head>
<body>
	<center>
	<div class="zoom_img" >
		<br><br><br><br>

			<div style="text-align: center; width: 250px; height: 400px">
				<a href="{{ asset('images/closed.jpg') }}" class="thumbnail">
			      <img src="{{ asset('images/closed.jpg') }}" alt="thumbnail">
			    </a>
			</div>
			<br>
		<h1>PENDAFTARAN DITUTUP</h1>
	</div>
	</center>
</body>
</html>
