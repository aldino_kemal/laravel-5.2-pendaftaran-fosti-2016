<!DOCTYPE html>
    <head>
        <title>Cara Pendaftaran FOSTI</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link href="{{ asset('images/icn.png') }}" rel="shortcut icon" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/caradaftar.css') }}">
        <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
        <noscript>
        <center>        
            <div style="margin-top:200px; font-size:30px">Enable JavaScript for best performance</div> 
            <style>form { display:none; }</style>
        </center>
        </noscript>
    </head>

    <body>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel body" style="padding-left:20px">
                    <!-- <div class="alert alert-info custom-flat"> -->
                        <h2 align="center">Cara Pendaftaran</h2>
                        <br><br>                        
                        <h4 style="padding-left:20px">Step 1</h4>
                        <p>Isi semua data yang disediakan pada form <a href='{{url("/")}}'>pendaftaran</a>, nah untuk upload foto pastikan foto berekstensi jpg/png/jpeg ukuran maksimal 1024KB atau setara dengan 1MB, bagi yang <b>tidak memiliki pasfoto</b> dapat diganti dengan foto closeup muka (wajah harus jelas)<br>
                        jika terjadi kesalahan saat melakukan pendaftaran akan ada pemberitahuan error sepert <b>INTERNAL SERVER ERROR</b> hubungi CP atau datang keSEKRE FOSTI 
                        </p>
                        <a href="{{ asset('images/caradaftar/langkah1.png')}}" target="_blank"><img src="{{ asset('images/caradaftar/langkah1.png')}}" width=800px></a> 
                        <br><br>

                        <h4 style="padding-left:20px">Step 2</h4>
                        <p>Setelah melalui step 1 kamu jika berhasil kamu akan di redirect kehalaman ini untuk mendownload formulir pendaftaran. Pada tahap ini kamu sudah <b>berhasil</b> mendaftar secara online</p>
                        <a href="{{ asset('images/caradaftar/langkah2.png')}}" target="_blank"><img src="{{ asset('images/caradaftar/langkah2.png')}}" width=800px></a> 
                        <br><br>

                        <h4 style="padding-left:20px">Step 3</h4>
                        <p>Jika kamu lupa download pada step 2, kamu juga dapat download formulir dengan membuka email yang terdaftar</p>
                        <a href="{{ asset('images/caradaftar/langkah3.png')}}" target="_blank"><img src="{{ asset('images/caradaftar/langkah3.png')}}" width=800px></a> 
                        <br><br>

                        <h4 style="padding-left:20px">Step 4</h4>
                        <p>Jangan lupa untuk <b>mencetak</b> form pendaftaran dan cantumkan <b>fotocopy</b> KTM / KTM sementara yang <b>harus dibawa saat hari H kegiatan tanggal 25 September 2016</b><br>
                        UNTUK INFO LEBIH LANJUT AKAN DIINFORMASIKAN LEWAT SMS...JADI PASTIKAN NOMOR YANG KALIAN ISIKAN <b>BENAR</b>
                        </p>
                        <a href="{{ asset('images/caradaftar/langkah4.png')}}" target="_blank"><img src="{{ asset('images/caradaftar/langkah4.png')}}" width=800px></a> 
                        <br><br>

                    <!-- </div> -->
                </div>
            
            </div>
        </div>

    </body>