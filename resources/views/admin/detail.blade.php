@extends('admin.admin_master')
@section('title','Admin | Detail | '.$nim)
@section('intro')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/detail.css') }}">
@endsection
@section('content')
<div class="container">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="panel panel-primary custom-flat" style="margin-top:3em">
			<div class="panel-heading custom-flat">
				<b>
				<a href="{{ url('/admin') }}" style="color:#FFFFFF">
					<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
				</a>
				<center>
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Detail </b>
				</center>
			</div>
			<div class="panel-body">
				<table>
					<tr>
						<td>Nama </td>
						<td>{{ $datas->nama }} </td>
					</tr>
					<tr>
						<td>Nim </td>
						<td>{{ $datas->nim }} </td>
					</tr>
					<tr>
						<td>Jurusan </td>
						<td>{{ $datas->jurusan }} </td>
					</tr>
					<tr>
						<td>Tanggal Lahir </td>
						<td>{{ $datas->tanggal_lahir }} </td>
					</tr>
					<tr>
						<td>Angkatan </td>
						<td>{{ $datas->angkatan }} </td>
					</tr>
					<tr>
						<td>Nomor Tepelon </td>
						<td>{{ $datas->no_telepon }} </td>
					</tr>
					<tr>
						<td>Email </td>
						<td>{{ $datas->email }} </td>
					</tr>
					<tr>
						<td>Hoby </td>
						<td>{{ $datas->hoby }} </td>
					</tr>
					<tr>
						<td>Motivasi </td>
						<td>{{ $datas->motivasi }} </td>
					</tr>
					<tr>
						<td>Alamat Rumah </td>
						<td>{{ $datas->alamat_rumah }} </td>
					</tr>
					<tr>
						<td>Asal Sekolah </td>
						<td>{{ $datas->asal_sekolah }} </td>
					</tr>
					<tr>
						<td>Tangal Daftar </td>
						<td>{{ $datas->created_at }} </td>
					</tr>
				</table>
				<div class="right-side">
					<!-- <img src="{{ asset('images/pasfoto') }}/{{$datas->nim.'.jpg'}}"> -->
		<?php dd(file_exists(public_path('images/pasfoto'.'/'.$datas->nim.'.jpg'))) ?>
		@if(file_exists(public_path('images/pasfoto'.'/'.$datas->nim.'.jpg')))
			<img src="{{asset('images/pasfoto')}}/{{$datas->nim}}.jpg" style="margin:50px 0 0 00px">
		@else
			<img src="{{asset('images/pasfoto')}}/Icon-user.png" style="margin:50px 0 0 00px">
		@endif
					<br><br>
					<a href={{url('berhasil/'.$nim.'/'.md5('formulirpendaftaran'))}} target="_blank" class="btn btn-default" style="margin-left:5px">Download</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection