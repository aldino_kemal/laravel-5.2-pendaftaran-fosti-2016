@extends('admin.admin_master')
@section('title','Admin | Delete | '.$nim)
@section('intro')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/delete.css') }}">
@endsection
@section('content')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=0.7">
<center>
<div class="container">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<div class="panel panel-danger custom-flat">
			<div class="panel-heading custom-flat"><b><span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Delete Data</b></div>
			<div class="panel-body">
				<form method='post' action="{{ url('admin/delete',$nim) }}" class="horizontal" role='form' name='form-delete'>
					{{ csrf_field() }}
					<table>
						<tr>
							<td>Nama</td>
							<td style="padding-left: 10px">{{ $datas->nama }}</td>
						</tr>
						<tr>
							<td>Nim</td>
							<td style="padding-left: 10px">{{ $datas->nim }}</td>
						</tr>
						<tr>
							<td>Dibuat</td>
							<td style="padding-left: 10px">{{ $datas->created_at }}</td>
						</tr>
					</table>
					<h3>Anda Yakin Akan Menghapus <br>"{{ $datas->nama }}"<br> Secara Permanen dari Database ?</h3>
					<br>
					<a href={{url('/admin')}} class="btn btn-default" style="float: left" ><span class="glyphicon glyphicon-menu-left" aria-hidden="true" style="font-size: 12px"></span>Back</a>
					<button class="btn btn-danger" name="delete_data" style="float:right"><span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Delete
					
				</form>
			</div>
		</div>
	</div>
</div>
</center>
@endsection