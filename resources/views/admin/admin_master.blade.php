<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link href="{{ asset('images/icn.png') }}" rel="shortcut icon" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
	@yield('intro')

</head>
<body>

@yield('content')

</body>

</html>