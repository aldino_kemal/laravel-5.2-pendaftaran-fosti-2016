<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/admin/login_admin.css')}}"/>
    <meta name="viewport" content="width=device-width, initial-scale=0.5">
    <link href="{{ asset('images/icn.png') }}" rel="shortcut icon" type="image/x-icon">
</head>
<body>

@if (count($errors) > 0)
<center>
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</center>
@endif
	<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <img class="profile-img" src="{{ asset('images/Fosti.png') }}"
                    alt="">
                <form class="form-signin" method="post" action="{{ action('AdminPostController@login') }}">
                {!! csrf_field() !!}
                <input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus>
                <input type="password" class="form-control" name="password" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Sign in</button>

                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>