@extends('admin.admin_master')
@section('title','Admin | Home')
@section('intro')
<!-- bootstrap -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/home.css') }}">
<!-- jqury -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- datatables -->
<link rel='stylesheet' href='https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css'>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#pagination').DataTable();
	} );
</script>
@endsection
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/style_admin.css') }}">
<div class="container main" style="margin-top:10px">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="alert alert-info custom-flat">
				<h2>Database calon anggota FOSTI 2016</h2>
			</div>
			<div class="table-responsive">
				<table border="1" class="table table-striped table-bordered" id="pagination" width="100%">
					<thead>
						<tr>
							<th>NO</th>
							<th>NIM</th>
							<th>NAMA</th>
							<th>JURUSAN</th>
							<th>NO TEPLEPON</th>
							<th>JENIS KELAMIN</th>
							<th>TANGAL DAFTAR</th>
							<th>AKSI</th>
							<th>STATUS</th>
							
						</tr>
					</thead>
					<?php $no=1 ;?>
					@foreach($datas as $user)
					@if($user->nim != "admin")
					<tr>
						<td>{{ $no }}</td>
						<td>{{ $user->nim }}</td>
						<td>{{ $user->nama }}</td>
						<td>{{ $user->jurusan }}</td>
						<td>{{ $user->no_telepon }}</td>
						<td>{{ $user->gender }}</td>
						<td>{{ $user->created_at }}</td>
						
						<td><a href='{{ url('admin/delete/'.$user->nim) }}'>Delete</a> | <a href='{{ url('admin/detail/'.$user->nim) }}' target='_blank'>Detail</a> <br> </td>
						<td>null</td>
					</tr>
					<?php $no++ ;?>
					@endif
					@endforeach
				</table>
			</div>

			<form action="{{ action('AdminPostController@logout') }}" method="post" name="logout">
				{{ csrf_field() }}
				<a href="{{ url('admin/exportExcel') }}" class="btn btn-success">Export Excel</a>
				{!! Form::button('<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout',['class'=>'btn btn-danger','type'=>'submit','style'=>'float: right; width: 100px; margin: 0 0 0 5px']) !!}
			</form>
		</div>
	</div>
</div>
@endsection