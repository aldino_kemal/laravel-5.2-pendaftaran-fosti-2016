<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPendaftaranOnline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->string('nim',10);
            $table->primary('nim');
            $table->string('email',255);
            $table->string('jurusan',255);
            $table->string('angkatan',5);
            $table->string('nama',255);
            $table->string('no_telepon',255);
            $table->string('hoby',255);
            $table->string('motivasi',255);
            $table->string('asal_sekolah',255);
            $table->string('alamat_rumah',255);
            $table->string('gender',20);
            $table->string('tanggal_lahir',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pendaftarans');
    }
}
