<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Pendaftaran;
use App\Login_admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

	    $this->call(PendafOnlineSeeder::class);
        $this->call(LoginAdminSeeder::class);
	    
	    Model::reguard();
    }
}
