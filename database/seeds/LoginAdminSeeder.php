<?php

use Illuminate\Database\Seeder;

class LoginAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('login_admins')->insert([
    		[
            'username' => 'admin',
            'password' => bcrypt('fosti2104'),
			'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
    		],
    	]);
    }
}
