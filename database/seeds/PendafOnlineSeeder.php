<?php

use Illuminate\Database\Seeder;

class PendafOnlineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pendaftarans')->insert([
        		[
	            'nim' => 'L200140132',
	            'email' => 'aldinokemal2104@gmail.com',
	            'jurusan' => 'INFORMATIKA',
	            'angkatan' => '2014',
	            'nama' => 'ALDINO KEMAL ADI GUMAWANG',
	            'no_telepon' => '08965024091',
	            'hoby' => 'BELAJAR',
	            'motivasi' => 'BELAJAR',
	            'asal_sekolah' => 'SMA N 1 BOYOLALI',
	            'alamat_rumah' => 'BOYOLALI',
	            'gender' => 'LAKI - LAKI',
	            'tanggal_lahir' => '1997-04-21',
				'updated_at' => date('Y-m-d H:i:s'),
	            'created_at' => date('Y-m-d H:i:s'),
        		],
        		[
	            'nim' => 'L200140162',
	            'email' => 'rasyid_fajar_n@yahoo.com',
	            'jurusan' => 'INFORMATIKA',
	            'angkatan' => '2014',
	            'nama' => 'RASYID FAJAR NUGARHA',
	            'no_telepon' => '08965024091',
	            'hoby' => 'BELAJAR',
	            'motivasi' => 'BELAJAR',
	            'asal_sekolah' => 'SMA N 2 SURAKARTA',
	            'alamat_rumah' => 'JEPARA',
	            'gender' => 'LAKI - LAKI',
	            'tanggal_lahir' => '1997-04-21',
				'updated_at' => date('Y-m-d H:i:s'),
	            'created_at' => date('Y-m-d H:i:s'),
        		],        	
        	]);
    }
}
